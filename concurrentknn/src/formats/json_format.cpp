/* 
 * File:   json_format.cpp
 * Author: eisenheim
 * 
 * Created on 22 de marzo de 2017, 23:57
 */

#include "formats/json_format.h"

JSONFormat::JSONFormat(string path, string dataFieldName):File(path) {
    std::cout << "This is a NEW JSON file" << std::endl;
}

JSONFormat::JSONFormat(const JSONFormat& orig):File(orig) {
    std::cout << "This is a COPIED JSON file" << std::endl;
}

JSONFormat::~JSONFormat() {
    std::cout << "This is a DESTROYED JSON file" << std::endl;
}

void JSONFormat::process() {
    std::cout << "This is a JSON being processed" << std::endl;
}

void JSONFormat::print() {
    std::cout << "This is a PRINTED JSON file" << std::endl;
}

void JSONFormat::write(string data) {
    std::cout << "This is a JSON file being written" << std::endl;
}
