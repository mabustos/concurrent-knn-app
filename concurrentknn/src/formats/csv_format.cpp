/* 
 * File:   csv_format.cpp
 * Author: eisenheim
 * 
 * Created on 22 de marzo de 2017, 23:57
 */

#include "formats/csv_format.h"

CSVFormat::CSVFormat(string path, string delimiter, bool header):File(path) {
    std::cout << "This is a NEW CSV file" << std::endl;
}

CSVFormat::CSVFormat(const CSVFormat& orig):File(orig) {
    std::cout << "This is a COPIED CSV file" << std::endl;
}

CSVFormat::~CSVFormat() {
    std::cout << "This is a DESTROYED CSV file" << std::endl;
}

void CSVFormat::process() {
    std::cout << "This is a CSV being processed" << std::endl;
}

void CSVFormat::print() {
    std::cout << "This is a PRINTED CSV file" << std::endl;
}

void CSVFormat::write(string data) {
    std::cout << "This is a CSV file being written" << std::endl;
}
