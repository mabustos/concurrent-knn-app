/* 
 * File:   plain_text_format.cpp
 * Author: eisenheim
 * 
 * Created on 22 de marzo de 2017, 23:57
 */

#include "utils/file.h"
#include "formats/plain_text_format.h"

PlainTextFormat::PlainTextFormat(string path, string delimiter): File(path) {
    std::cout << "This is a NEW PLAIN TEXT file" << std::endl;
}

PlainTextFormat::PlainTextFormat(const PlainTextFormat& orig): File(orig) {
    std::cout << "This is a COPIED PLAIN TEXT file" << std::endl;
}

PlainTextFormat::~PlainTextFormat() {
    std::cout << "This is a DESTROYED PLAIN TEXT file" << std::endl;
}

void PlainTextFormat::process() {
    std::cout << "This is a PLAIN TEXT FILE being processed" << std::endl;
}

void PlainTextFormat::print() {
    std::cout << "This is a PRINTED PLAIN TEXT file" << std::endl;
}

void PlainTextFormat::write(string data) {
    std::cout << "This is a PLAIN TEXT file being written" << std::endl;
}
