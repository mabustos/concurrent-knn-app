#include <iostream>
#include "data_processor/data_processor.h"
using namespace std;


 
template <class T> void DataProcessor::setInputContainer(DataContainer<T> container) {
    std::cout << "Input container set" << std::endl;
}

void DataProcessor::setPreOperations(vector<Operation> preOperations) {
    std::cout << "Preoperations set" << std::endl;
}

void DataProcessor::setOperation(Operation operation) {
    std::cout << "Operation set" << std::endl;
}

void DataProcessor::process() {
    std::cout << "Process OK!" << std::endl;
}
