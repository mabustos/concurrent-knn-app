#include <iostream>
#include <utils/file.h>

File::File(string path){
    std::cout << "This is FILE constructor with string name: " << path << std::endl;
}

File::File(const File& origin ){
    std::cout << "This is FILE copy by reference" << std::endl;
}

File::~File(){
    std::cout << "This is FILE destructor" << std::endl;
}

void File::process() {

}

void File::print() {

}

void File::write(string data) {

}