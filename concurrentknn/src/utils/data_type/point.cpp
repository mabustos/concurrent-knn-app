#include <iostream>
#include <utils/data_type/point.h>

class Point{
    int id;
    double distance;
public:
    Point(int id, double distance);
    void setDistance(double distance);
};

Point::Point(int id, double distance){
    this->id=id;
    this->distance=distance;
}

Point::setDistance(double distance){
    this->distance=distance;
}

