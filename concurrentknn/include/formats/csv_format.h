#ifndef   FWRK_CSV_FORMAT
#define   FWRK_CSV_FORMAT

#include <iostream>
#include <string>
#include "utils/file.h"

using namespace std;

class CSVFormat : public File{
        string delimiter;
        bool printHeader;
    public:
        CSVFormat(string path, string delimiter, bool header);
        CSVFormat(const CSVFormat& orig);
        ~CSVFormat();
        void process();
        void print();
        void write(string data);
};
#endif
