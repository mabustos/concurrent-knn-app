#ifndef   FWRK_JSON_FORMAT
#define   FWRK_JSON_FORMAT

#include <iostream>
#include <string>
#include "utils/file.h"

using namespace std;


class JSONFormat : public File{
        string dataFieldName;
    public:
        JSONFormat(string path, string dataFieldName);
        JSONFormat(const JSONFormat& orig);
        virtual ~JSONFormat();
        void process();
        void print();
        void write(string data);
};

#endif
