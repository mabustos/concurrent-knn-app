/* 
 * File:   formats.h
 * Author: eisenheim
 *
 * Created on 22 de marzo de 2017, 23:54
 */

#ifndef FORMATS_H
#define	FORMATS_H

#include "utils/file.h"
#include "formats/csv_format.h"
#include "formats/plain_text_format.h"
#include "formats/json_format.h"

#endif	/* FORMATS_H */

