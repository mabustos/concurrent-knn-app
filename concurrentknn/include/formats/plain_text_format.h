#ifndef   FWRK_PLAIN_TEXT_FORMAT
#define   FWRK_PLAIN_TEXT_FORMAT

#include <iostream>
#include <string>
#include "utils/file.h"

using namespace std;

class PlainTextFormat : public File{
        string delimiter;
    public:
        PlainTextFormat(string path, string delimiter);
        PlainTextFormat(const PlainTextFormat& orig);
        virtual ~PlainTextFormat();
        void process();
        void print();
        void write(string data);
        
};

#endif
