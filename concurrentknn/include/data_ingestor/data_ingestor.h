#ifndef   FWRK_DATA_INGESTOR
#define   FWRK_DATA_INGESTOR

#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include "../utils/file.h"

using namespace std;

template <class T>
class DataIngestor{
        vector<T> inputContainer;
    private:
        File* file;
    public:
        void setFile(File file);
        void ingestData();
        bool isDataValid() const;
        void printMetaData() const;
};

template <class T>
void DataIngestor<T>::setFile(File file){
    cout << "Setting file" << endl;
    //file=file;
}

template <class T>
void DataIngestor<T>::ingestData(){
    cout << "Ingesting data" << endl;
}

template <class T>
bool DataIngestor<T>::isDataValid() const{
    cout << "Is data valid?" << endl;
}

template <class T>
void DataIngestor<T>::printMetaData() const{
    cout << "Printing metadata" << endl;
}

#endif
