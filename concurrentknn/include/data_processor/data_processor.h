#ifndef   FWRK_DATA_PROCESSOR
#define   FWRK_DATA_PROCESSOR

#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include "../data_structures/data_container.h"
#include "../utils/operation.h"
#include "../utils/time_counter.h"

using namespace std;

class DataProcessor{
    TimeCounter timer;
    public:
        template <class T> void setInputContainer(DataContainer<T> container);
        void setPreOperations(vector<Operation> preOperations);
        void setOperation(Operation operation);
        void process();
};

#endif
