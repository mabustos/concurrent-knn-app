#ifndef   FWRK_FILE
#define   FWRK_FILE

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class File : public ifstream{
    protected:
        string path;
        File(string path);
        File(const File& orig);
        virtual ~File();
    public:
        virtual void process();
        virtual void print();
        virtual void write(string data);
};

#endif
