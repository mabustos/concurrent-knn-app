#ifndef POINT_H
#define	POINT_H

#include <iostream>

class Point{
    int id;
    double distance;
public:
    Point(int id, double distance);
    void setDistance(double distance);
};

#endif	

