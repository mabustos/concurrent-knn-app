#ifndef   FWRK_TIME_COUNTER
#define   FWRK_TIME_COUNTER

#include <iostream>
#include <time.h>

using namespace std;

class TimeCounter{
    private:
        clock_t startTimeStamp;
        clock_t stopTimeStamp;
    public:
        void start();
        void stop();
};

#endif
