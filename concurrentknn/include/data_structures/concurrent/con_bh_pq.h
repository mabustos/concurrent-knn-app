#include <iostream>
#include <vector>
#include <algorithm>

template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    class priority_queue {
    protected:
    Container c;
    Compare comp;

    public:
    explicit priority_queue(const Container& c_  = Container(),
                            const Compare& comp_ = Compare());
    bool empty()       const;
    std::size_t size() const;
    const T& top()     const;
    void push(const T& x);
    void pop();
};

template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    priority_queue<T> priority_queue<T> :: priority_queue(const Container& c_  = Container(),
                                                          const Compare& comp_ = Compare()){
        c=c_;
        comp=comp;
        std::make_heap(c.begin(), c.end(), comp);
    }
};


template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    bool priority_queue :: empty() const {
        return c.empty();
    }
};


template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    std::size_t priority_queue<T> :: size() const {
        return c.size();
    }
};


template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    const T& priority_queue :: top() const {
        return c.front();
    }
};


template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    void priority_queue :: push(const T& x){
        c.push_back(x);
        std::push_heap(c.begin(), c.end(), comp);
    }

void pop(){
    std::pop_heap(c.begin(), c.end(), comp);
    c.pop_back();
}
};

template <class T, class Container = std::vector<T>,
class Compare = std::less<T> >
    void priority_queue :: pop(){
        std::pop_heap(c.begin(), c.end(), comp);
        c.pop_back();
    }
};




