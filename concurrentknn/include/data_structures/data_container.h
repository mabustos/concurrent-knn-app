#ifndef   FWRK_DATA_CONTAINER
#define   FWRK_DATA_CONTAINER

#include <iostream>

using namespace std;

template <class T>
class DataContainer{
    virtual T pop();
    virtual void push();
    virtual T top() const;
    virtual T popPush(T elem);
};

#endif
