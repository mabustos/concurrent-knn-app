#ifndef   FWRK_DATA_PRESENTATOR
#define   FWRK_DATA_PRESENTATOR

#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include "../utils/file.h"
#include "../data_structures/data_container.h"

using namespace std;

class DataPresentator{
    File* format;
    public:
        template <class T> void printOutput(DataContainer<T> container);
};

#endif
