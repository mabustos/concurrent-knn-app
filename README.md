# Concurrent Data Structures for Solving KNN Queries #

* Author: Matías Bustos
* Revisor: Ricardo Barrientos

## Description ##

The following C++ project offers several implementations of concurrent data structures for solving KNN queries on x86 architectures.
