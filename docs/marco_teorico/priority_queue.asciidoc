= TAD - Cola con prioridades

Doc Writer <doc@example.com>

La inserción de arquitecturas multinúcleo de memoria compartida en microprocesadores [referencia] ha dado paso a la creación de nuevos modelos de programación que permiten al desarrollador explotar el paralelismo sin necesidad de trabajar a nivel de instrucciones. A continuación, se dará una breve reseña de cómo esta caracterítica ha convertido en un factor decisivo en el diseño de aplicaciones, la tecnología actual que lo sustenta y los modelos de programación más populares.

== Evolución de los microprocesadores modernos

=== Arquitecturas de un núcleo

=== Arquicturas multinúcleo


== Otras tecnologías

=== GPU

=== Coprocesadores

== Modelos de programación

=== OpenMP

=== Intel Building Blocks

=== OPTIK



[source,ruby]
puts "Hello, World!"
